Portmod Development Guide
=========================

Note that some of this information refers to development of Portmod itself,
while other parts refers to the creation of portmod packages and package
repositories.

Package Development
-------------------

.. toctree::
   :maxdepth: 2

   guidelines
   manifest
   use-flags
   modules
   archives

Portmod Development
-------------------

.. toctree::
   :maxdepth: 2

   l10n
   setup
